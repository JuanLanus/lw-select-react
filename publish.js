#! /usr/local/bin/node

const { execSync } = require( 'child_process' );
const fs = require( 'fs' );

/*--------------------------------------------------------------*/
/*
 * package.json files:
 *  There is the normal project one, and another to be publihed
 *  to npm with different information.
 *  In both files the current version property is updated.
 * README.md files:
 *  The main README.md file is in the ReactJS project, in the HTML
 *  there is a symlink to the ReactJS original, and a small text
 *  file explaining where to get the original.
 *  The HTML project has another file ,/public/README_PROTO.md that
 *  is there because it was not deleted.
 *  The main README is published to npm with the internal links
 *  edited, due to a difference in the MarkDown lingo used by
 *  BitBucket.
*/

let currentVersion = 'v0.0.0';

const versionLevels = [ 'major', 'minor', 'patch' ];
let runningCommand = 'none yet';

/*--------------------------------------------------------------*/
// shared resources

const runCommand = ( comm, silent ) => {
  runningCommand = comm;
  if( ! silent){
    console.log( 'command "' + comm + '"' );
  };
  const z = execSync(
    comm,
    { encoding: 'utf8' }
  )
  .trim();
  if( ! silent){
    console.log( 'returns: ' + z );
  };
  return z;
};

/****************************************************************/

let z = '';
z = runCommand( 'pwd', true );

const packageJson = require( z + '/package.json' );
// console.log( '\n\npackage.json:', JSON.stringify( packageJson, null, 2 ) + '\n\n' );

const packageJsonNpm = require( z + '/package-npm.json' );
// console.log( 'packageJsonNpm:', JSON.stringify( packageJsonNpm, null, 2 ) + '\n\n' );

const pd = require( z + '/publishData.json' );

const dryRun = ( process.env.DRYRUN === 'true' ? '--dry-run' : '' );
pd.title = process.env.TITLE;
fs.writeFileSync( z + '/publishData.json', JSON.stringify( pd, null, 2 ));

console.log( `\n\n\n**********************************************************
\n${ pd.title } ${ dryRun ? '   🡆 DRY RUN' : ''} \n` );

// console.log( '\npublishData.json:', JSON.stringify( pd, null, 2 ) + '\n\n' );

/****************************************************************/

const publishNewVersion = () => {

  try{
    console.log( '\n🡆 Ensure we are in the project\'s directory ' + pd.projectDir );
    z = runCommand( 'pwd' );
    const theDirName = z.split( '/' ).pop();
    if( theDirName !== pd.projectDir ){
      console.log( '\nTo run this script need to be in dir ' + pd.projectDir );
      return;
    };

    /*--------------------------------------------------------------*/
    console.log( '\n🡆 Ensure we are in the publish branch ' + pd.publishBranch );
    z = runCommand( 'git branch --show-current' );
    if( z !== pd.publishBranch ){
      console.log( '\nYou can only publish branch ' + pd.publishBranch );
      return;
    };

    /*--------------------------------------------------------------*/
    console.log( '\n🡆 Ensure that the working tree is clean ' );
    z = runCommand( 'git status -s' );
    if( z.trim() ){
      console.log( '\nThere are uncommitted files\n' );
      return;
    };

    /*--------------------------------------------------------------*/
    console.log( '\n🡆 find out the latest tag' );
    z = runCommand( 'git describe --tags --abbrev=0' );
    currentVersion = z;

    /*--------------------------------------------------------------*/
    console.log( '\n🡆 Bump the version (latest is ' + currentVersion + ')' );
    const versionLevel = process.env.LEVEL;
    console.log( 'versionLevel: ' + versionLevel );
    if( ! versionLevels.includes( versionLevel )){
      console.log( `Version level "${ versionLevel }" invalid, must be one of ${ versionLevels.toString()}` );
      return;
    }
    z = runCommand( `semver --increment ${ versionLevel } ${ currentVersion }` );
    pd.newVersion = 'v' + z;

    /*--------------------------------------------------------------*/
    console.log( '\n🡆 Write package.json files with the updated version' );
    z = runCommand( `
      mv package.json package-OLD.json
      mv ${ pd.publishedDir }package.json package-npm-OLD.json 
    ` );
    packageJson.version = pd.newVersion;
    fs.writeFileSync( 'package-NEW.json', JSON.stringify( packageJson, null, 2 ));
    packageJsonNpm.version = pd.newVersion;
    fs.writeFileSync( 'package-npm-NEW.json', JSON.stringify( packageJsonNpm, null, 2 ));
    console.log( `written package-NEW.json and package-npm-NEW.json` );

    /*--------------------------------------------------------------*/
    console.log( '\n🡆 Log the commits since the previous tag (rel notes)' );
    z = runCommand( `git log --decorate ${ currentVersion }..HEAD` );
    z = pd.versionMessage + '\n\n' + z;
    if( ! fs.existsSync( './relnotes' )){
      fs.mkdirSync( 'relnotes' );
    }
    fs.writeFileSync( `relnotes/release-notes.${ pd.newVersion }.txt`, z );

    /*--------------------------------------------------------------*/
    console.log( `\n🡆 Build` );
    z = runCommand( pd.buildStepCommands );

    /*--------------------------------------------------------------*/
    console.log( `\n🡆 Commit, create tag ${ pd.newVersion }, push` );
    z = runCommand( `
      # replace updated package.json files
      mv package-NEW.json package.json
      mv package-npm-NEW.json ${ pd.publishedDir }package.json
    ` );
    z = runCommand( `
      # run the commit
      git add . --all
      git commit ${ dryRun } -m "${ pd.newVersion } ${ pd.versionMessage }"
      git tag -a ${ pd.newVersion } -m "${ pd.versionMessage }"
      git push --follow-tags ${ dryRun } origin master
      ${ dryRun ? "#" : "" } git tag --delete ${ pd.newVersion }
    ` );

    /*--------------------------------------------------------------*/
    console.log( `\n🡆 Publish to npm` );
    z = runCommand( `
      cd ${ pd.publishedDir }
      npm publish --otp=$NPMOTP ${ dryRun } .
      cd -
    ` );

    /*--------------------------------------------------------------*/
    // Rollback changes done to the filesystem
    if( dryRun ){
      console.log( `\n🡆 DRY RUN: restore package.json files, delete relnotes` );
      z = runCommand( `
        git reset
        git restore public/lw-select/package-npm.json
        mv package-OLD.json package.json
        # mv package-npm-OLD.json ${ pd.publishedDir }package-npm.json
        rm relnotes/release-notes.${ pd.newVersion }.txt
      ` );
    } else {
      console.log( `\n🡆 Delete backup package.json files` );
      z = runCommand( `
        rm .json package-OLD.json
        rm package-npm-OLD.json 
      ` );
    };
    /*--------------------------------------------------------------*/
    console.log( `\n🡆 Process completed ${ dryRun ? '      DRY RUN' : ''}` );
    console.log( '**********************************************************' );

  } catch( error ){
    console.log( '\n\nFailed' );
    console.log( runningCommand );
    console.log( error );
  }
};

/****************************************************************/

publishNewVersion();

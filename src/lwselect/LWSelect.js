import React, { Component } from 'react';
import styles from './LWSelect.module.css';
import buildSearchArrayFn from './LWBuildSearchArrayFn';

class LWSelect extends Component {

  // ref to close the list of other instance
  static closeLWS = null;

  state = {
    // values returned to the application
    valueIndex: null, // index of the selected item in the list
    value: null,      // ref to the selected item
    valueText: '',    // the text displayed in the list

    searchArray: [],
    itemDescriptions: [],

    // search text input
    searchText: '',
    searchTag: 0,

    // state of the display list
    isListOpen: false,
    isListLoaded: false,

    // status of the display list scroll
    scroll: {
      highlightedLI: null, // ref to the highlighted LI
      scrolling: false, // scroll data is valid
    },

    // messages
    noMatchesMsg: 'no matches found',

  };

  // data for the displayed list
  availableHeight = null;
  itemsPerChunk = 100;

  removeAccents = ( text ) => {
    return text
      .normalize( 'NFD' )
      .replace( /[\u0300-\u036f]/g, '' );
  };

  /*--------------------------------------------------------------*/
  // Lifecycle methods

  constructor( props ){
    super();
    // outer container
    this.getThisNode = React.createRef();
    // search input text ans open/cloe button
    this.getSearchInput = React.createRef();
    this.getDropButton = React.createRef();
    // display list container (drop)
    this.getULContainer = React.createRef();

    // override default msg text
    if( props.hasOwnProperty( 'noMatchesMsg' )){
      this.state.noMatchesMsg = props.noMatchesMsg;
    };
  };

  componentDidMount(){
    const props = this.props;
    const listSource = props.listSource;

    // build the search and display lists
    this.buildDisplayList( props, this.state );
    const searchArray = buildSearchArrayFn( listSource, props.listSearchProps );
    this.setState({ searchArray: searchArray });

    // leverage the pre-selected value if any
    this.handleInitialValues( props, this.state );

    // handle readonly control
    if( props.hasOwnProperty( 'readonly' )){
      this.handleReadOnly( props.readonly );
    };

    // on resize, recalculate the vert space available for the drop
    window.addEventListener(
      'resize',
      ( event ) => {
        this.invalidateAvailableHeight();
        if( this.state.isListOpen ){
          const theULContainer = this.getULContainer.current;
          theULContainer.style.maxHeight = this.calculateAvailableHeight() + 'px';
        };
      }
    );
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.invalidateAvailableHeight );
  };

  /*--------------------------------------------------------------*/
  handleReadOnly = ( ro ) => {
    if( ro === true || ro === false ){
      // OK
    } else {
      if( ro === 'readonly' ){
        ro = true;
      } else {
        console.warn( '[LWSelect] invalid readonly prop value ignored "'
          + ro + '"/nValid values are true, false and "readonly"' );
        return;
      };
    };
    const theOuterContainer = this.getThisNode.current;
    if( ro ){
      theOuterContainer.classList.add( styles.lwNoHover ); 
    } else {
      theOuterContainer.classList.remove( styles.lwNoHover ); 
    };
  };

  /*--------------------------------------------------------------*/
  // returns all the the display list texts in an array
  buildDisplayList = ( props, state ) => {
    const listSource = this.props.listSource;
    const listDisplayProps = this.props.listDisplayProps;

    // if the displayList prop is a string, make it a function
    if( typeof props.listDisplayProps === 'string' ){
      this.listDisplayProps = ( item ) => {
        return item[ props.listDisplayProps ]
      }
    } else {
      this.listDisplayProps = props.listDisplayProps;
    }

    // build the display-strings array
    const m = listSource.length;
    const itemDescriptions = new Array( m );
    let k;
    try {
      for( k = 0; k < m; k++ ){
        itemDescriptions[k] = listDisplayProps( listSource[k], k );
      }
    } catch( err ){
      console.warn( 'lw-select: display string formatting failed at index ' + k
      + ' fn: ' + listDisplayProps, err );
    };

    this.setState({ itemDescriptions: itemDescriptions });

    // if no initial value load the full list (still hidden)
    if( this.state.valueIndex === null ){
      const n = this.state.itemDescriptions.length;
      const matches = Int8Array.from({ length: n }, ( v, i ) => 1 );
      this.renderTheList( matches, n );
    }
  };

  /*--------------------------------------------------------------*/
  // Format the HTML for the list using the matches flags
  // array and the display list
  renderTheList( matches, nMatch ){
    // set the DOM element for the new list
    const theNewUL = document.createElement( 'UL' );
    const theULContainer = this.getULContainer.current;
    const theOldUL = theULContainer.firstChild;
    theULContainer.replaceChild( theNewUL, theOldUL );

    // if there are no matches, return
    if( ! nMatch ){
      const z = '<LI>' + this.state.noMatchesMsg + '</LI>';
      theNewUL.insertAdjacentHTML( 'beforeend', z );
      return;
    };

    // flag the selected item in the matches list
    if( this.isSelectionOutstanding() ){
      matches[ this.state.valueIndex ] = -matches[ this.state.valueIndex ];
    };

    // invalidate saved scroll state
    this.listScrollReset();

    // start formatting the HTML: trigger the first chunk
    setTimeout(() => { 
      this.formatNextChunk( this.state.searchTag, 0, matches, theNewUL );
    });

    return;
  };

  /*--------------------------------------------------------------*/
  // Format the HTML for the next chunk of the list using
  // the matches flags array and the display list
  formatNextChunk = ( searchTag, i, matches, theNewUL ) => {
    /*
     * searchTag: a number that is uncremented whenever the change
     *    event is fired in the search input
     * i: position in the matches list where to start this chunk
     * matches: array of zeros or ones, output of the search
     * theNewUL: the UL in the DOM where to store the HTML chunks
    */
    const n = matches.length;
    const itemDescriptions = this.state.itemDescriptions;
    const itemsPerChunk = this.itemsPerChunk;

    // If a search event happened, stop formatting
    if( searchTag !== this.state.searchTag ){
      return;
    };

    // do the formatting
    let z = ''; // chunk HTML text
    let j = 0; // index in the chunk
    for ( ; i < n && j < itemsPerChunk; i++ ){
      if( matches[i] ){ // if item #i was matched by search
        z += (
          (( matches[i] === -1 ) // if it's the selected one
          ? '<LI data-idx="' + i + '" class="' + styles.lwListSelected + '">'
          : '<LI data-idx="' + i + '">' )
        + itemDescriptions[i] + '</LI>' );
        j++;
      }
    };

    // Render this chunk
    if( z ){
      theNewUL.insertAdjacentHTML( 'beforeend', z );
    };

    if( i >= n ){
      // Last chunk: scroll the selected item into view, if any
      const theSelectedLI = this.getSelectedListItem();
      theSelectedLI && theSelectedLI.scrollIntoView({
        block: 'center'
      });
    } else {
      // Schedule next chunk
      setTimeout(() => { 
        this.formatNextChunk( searchTag, i, matches, theNewUL );
        return;
      });
    };
  };

  /*--------------------------------------------------------------*/
  // The initial value can be in one or more of the three
  // valueXXX members of the props object, check them all
  handleInitialValues = ( props, state ) => {
    let i; // will point to the selected item in the list, if any

    // check props.value
    if( props.value && typeof props.value === 'object' ){
      i = props.listSource.findIndex( item => item === props.value );
      if( i < 0 ){ // not found
        console.warn( 'lw-select: initial value ignored', props.value );
        this.setState({ value: null });
      } else {
        this.setDisplayText( i, props, state );
        return;
      }
    }

    // check props.valueIndex
    i = props.valueIndex;
    if( typeof i === 'number' && i >= 0 && i < props.listSource.length ){
      this.setDisplayText( i, props, state );
      return;
    } else {
      if( ! ( i === null ) && ! ( i === undefined )){
        console.warn( 'lw-select: initial value ignored', props.valueIndex );
        this.setState({ valueIndex: null });
      }
    }

    // check props.valueText
    const t = props.valueText;
    if( ! t ){
      this.setState({ valueText: null });
    } else {
      i = props.listSource.findIndex(
        ( item, i ) => t === this.runListDisplayProps( props.listSource[i] )
      );
      if( i < 0 ){ // not found
        console.warn( 'lw-select: initial value ignored', t );
        this.setState({ valueText: null });
      } else {
        this.setDisplayText( i, props, state );
      }
    }
  };

  // update the UI to reflect the initial value selected
  setDisplayText = ( i, props, state ) => {
    const displayText = this.runListDisplayProps( props.listSource[i], i );
    // tidy up return values
    this.setState({
      valueIndex: i,
      value: props.listSource[i],
      valueText: displayText,
      searchText: displayText,
    });

    // update the search input UI
    const theSearchInput = this.getSearchInput.current;
    theSearchInput.value = displayText;
    theSearchInput.classList.add( styles.lwSearchTextSelected );

    // build a single-item display list with only the selected item
    const theULContainer = this.getULContainer.current;
    const z = '<UL><LI data-idx="' + i + '" class="' + styles.lwListSelected
    + '">' + displayText + '</LI></UL>';
    theULContainer.innerHTML = z;
  };

  /*--------------------------------------------------------------*/
  // trigger search in response to changes in the search string
  doSearch = ( event ) => {

    this.setState({
      searchText: event.target.value
    });

    const selectedListItem = this.getSelectedListItem();
    this.deSelectListItem( selectedListItem );
    this.showTheList();

    const searchText = event.target.value.trim();

    // if searchText has words, it will be a regex array
    const words = searchText.trim().split( /\s\s*/ );
    const isMultiWord = ( words.length > 1 );

    // if searchText is a single word that matches ^#\d+$ then the search
    // points to an item by its index, it's like "#1234"
    let isSearchByNumber = false;
    let searchedNumber;
    if( searchText.match( /^#\d+$/ )){
      // hide all items, show the single one pointed by the number (if exists)
      isSearchByNumber = true;
      searchedNumber = Number( searchText.replace( '#', '' ));
    };

    // make regexes with the search text
    let rexp;
    if( isMultiWord ){
      rexp = words.map( w => new RegExp( this.removeAccents( w.toLowerCase() )))
    } else {
      if( ! searchText.trim()){ // TODO: searchText is already trimmed
        rexp = /./; // match all items
      } else {
        rexp = new RegExp( this.removeAccents( words[0].toLowerCase() ));
      };
    };

    // match each list item's search text against the search query
    const searchArray = this.state.searchArray;
    let i = 0; // index for the search texts array
    const n = searchArray.length;
    let nMatch = 0; // matched items counter, for debugging
    let matches = new Int8Array( n ); // a matched flag per item
    let isMatch = false;
    event.target.classList.remove(
      styles.lwSearchTextNoMatch,
      styles.lwSearchTextSelected
    );

    if( ! searchText ){ // if no text all items match
      matches = Int8Array.from({ length: n }, ( v, i ) => 1 );
      nMatch = n;
    } else { // need to test item by item
      for( i = 0; i < n; i++ ){
        const searchArrayItem = searchArray[i];
        if( isMultiWord ){ // check the regexp array
          isMatch = true;
          for( let i = 0; i < rexp.length && isMatch; i++ ){
            isMatch = ( rexp[i].test( searchArrayItem ));
          };
        } else { // #nnn or single regex 
          if( isSearchByNumber ){
            isMatch = ( i === searchedNumber )
          } else {
            isMatch = ( rexp.test( searchArrayItem ));
          };
        };
        if( isMatch ){
          matches[i] = 1; // item #i will be displayed in the list
          nMatch++;
        }
      };

      if( ! nMatch ){
        event.target.classList.add( styles.lwSearchTextNoMatch );
      };
    };

    this.renderTheList( matches, nMatch ); // generate the HTML
    this.showTheList();
  };


  /*-------------------- manage the items list -------------------*/

  // returns the number of pixels available below the search text
  calculateAvailableHeight = () => {
    if( this.availableHeight === null ){
      const domRect = this.getThisNode.current.getBoundingClientRect();
      const viewportHeight = document.documentElement.clientHeight;
      this.availableHeight = viewportHeight - domRect.bottom;
      // const availableSpaceAbove = domRect.top;
    };
    return this.availableHeight;
  };

  invalidateAvailableHeight = () => {
    this.availableHeight = null;
  };

  // make the items list visible
  showTheList = () => {
    if( this.state.isListOpen ){
      return;
    };

    if( ! this.state.isListLoaded){
      if( this.state.valueIndex === null ){
        const n = this.state.itemDescriptions.length;
        const matches = Int8Array.from( { length: n }, ( v, i ) => 1 );
        this.renderTheList( matches, n );
      }
    };

    this.setState(
      {
        isListOpen: true,
        isListLoaded: true,
      },
      () => {
        // ensure this is the only opened select instance
        if( LWSelect.closeLWS && LWSelect.closeLWS !== this.hideTheList ){
          // close the other select drop and register this instance as opened
          LWSelect.closeLWS();
        };
        LWSelect.closeLWS = this.hideTheList;

        // now open this instance's drop
        const theULContainer = this.getULContainer.current;
        theULContainer.style.maxHeight = this.calculateAvailableHeight() + 'px';
        theULContainer.style[ 'z-index' ] = '10';
        // make it visible
        theULContainer.style.visibility = 'visible';
      }
    )
  };

  hideTheList = () => {
    if( ! this.state.isListOpen ){
      return;
    };
    this.setState(
      { isListOpen: false },
      () => {
        const theULContainer = this.getULContainer.current;
        theULContainer.style.visibility = 'hidden';
        theULContainer.style[ 'z-index' ] = '';
        // remove from the open drops list
        LWSelect.closeLWS = null;
      }
    )
  };

  toggleTheList = () => {
    if( this.state.isListOpen ){
      this.hideTheList();
    } else {
      this.showTheList();
    }
  };

  selectListItem = ( theItem ) => {
    console.log( 'selectListItem(): theItem: ', theItem );
    // move the selected highlight to this item
    const prevSelection = this.getSelectedListItem();
    prevSelection && prevSelection.classList.remove( styles.lwListSelected );
    theItem && theItem.classList.add( styles.lwListSelected );

    // show the item text in the search text field
    const theSearchInput = this.getSearchInput.current;
    theSearchInput.value = theItem.innerText;
    theSearchInput.classList.add( styles.lwSearchTextSelected );
    theSearchInput.focus();

    // update the to-be-returned values in state
    this.setState(
      {
        valueIndex: Number( theItem.dataset.idx ),
        valueText: theItem.innerText,
        value: this.props.listSource[ theItem.dataset.idx ],
        searchText: theItem.innerText,
      },
      () => {
        // call the onSelectionChanged parameter function if available
        if( typeof this.props.onSelectionChanged === 'function' ){
          try{
            this.props.onSelectionChanged( this.state.value, this.state.valueIndex,
              this.state.valueText );
          } catch( err ){
            console.warn( 'lw-select: onSelectionChanged() failed', err );
          }
        }
      }
    );

    // hide the list
    this.hideTheList();
  };

  deSelectListItem = ( theItem ) => { // argument theItem for now...
    if( ! theItem ){
      return;
    };

    // remove selected style of list item
    const prevSelection = this.getSelectedListItem();
    prevSelection && prevSelection.classList.remove( styles.lwListSelected );

    // clear the to-be-returned values in state
    this.setState({
      valueIndex: null,
      valueText: null,
      value: null,
    })

    // call the onSelectionChanged parameter function if available
    if( typeof this.props.onSelectionChanged === 'function' ){
      this.props.onSelectionChanged( this.state.value, this.state.valueIndex,
        this.state.valueText );
    }
  };

  // run the display list item formatting function inside try-catch
  runListDisplayProps = ( item, i ) => {
    try {
      return this.props.listDisplayProps( item, i );
    } catch( err ){
      console.warn( '[lw-select] display string formatting failed at i: ' + i
      + '\nfn: ' + this.listDisplayProps.toString() + '\nitem:', item, err );
    };
  };

  /*------------------- /manage the items list -------------------*/


  /*--------------------- event handlers -------------------------*/

  /*--------------------------------------------------------------*/
  // do search after a change in the search text
  handleSearchTextChange = ( event ) => {
    event.persist();
    this.setState(( state, props ) => ({
      searchTag: state.searchTag + 1,
      searchText: event.target.value
    }));
    this.doSearch( event );
    this.showTheList();
  };

  /*--------------------------------------------------------------*/
  // react to clicks in the open/close button

  handleListButtonClick = ( event ) => {
    if( event.target.nodeName === 'BUTTON'
    && ( ! this.props.readonly )){
      if( event.button === 0 ){
        this.toggleTheList();
        event.stopPropagation();
        // HTML adds class 'lwSearchTextActive' to .lwBox
        this.getSearchInput.current.focus();
      }
    }
  }


  /*--------------------------------------------------------------*/
  // react to keyboard in the search text
  handleSearchInputKeydown = ( event ) => {
    switch( event.key ){

      case 'Down':
      case 'ArrowDown':
        // highlight the first list/next LI item
        this.listScrollForward();
        break;

      case 'Up':
      case 'ArrowUp':
        // highlight the first last/prev LI item
        this.listScrollBack();
        break;

      case 'Enter':
        // on enter, if scroll is active then select the
        // highlighted item, and toggle the list visibility
        this.listScrollSelect();
        break;

      case 'Esc':
      case 'Escape':
        // close the list
        this.hideTheList();
        break;

      default:
        break;
    };
  };

  /*--------------------------------------------------------------*/
  // detect focusout of the container, to close the drop
  handleOutsideClick = ( event ) => {
    const theFringe = this.getThisNode.current;
    if( ! theFringe.contains( event.relatedTarget )){
      this.hideTheList();
    }
  };

  /*--------------------------------------------------------------*/
  // react to click in the search text input
  handleSearchInputClick = ( event ) => {
    if( event.target.nodeName === 'INPUT'
    && ( ! this.props.readonly )){
      if( event.button === 0 ){
        event.stopPropagation();
        this.getSearchInput.current.parentElement
          .classList.add( styles.lwSearchTextActive );
        this.showTheList();
      }
    }
  };

  /*--------------------------------------------------------------*/
  // detect mouse down in the items list, for item selection
  handleListMouseDown = ( event ) => {
    console.log( 'item clicked btn: ' + event.button, event.target );
    if( event.button === 0 && event.target.nodeName === 'LI' ){
      // save a ref to the clicked item, to select it later
      this.listItemToBeSelected = event.target;
      event.preventDefault();
    }
  };

  /*--------------------------------------------------------------*/
  // detect mouse up in the items list, for item selection
  handleListMouseUp = ( event ) => {
    // select the previously referenced list item
    console.log( 'mouse released' );
    if( event.button === 0 && event.target.nodeName === 'LI' ){
      this.selectListItem( this.listItemToBeSelected ); // (hided the list)
      delete this.listItemToBeSelected;
    }
  };

  /*--------------------------------------------------------------*/
  // close the drop when the user tabs out of the select
  handleBlur = ( event ) => {
  };

  /*-------------------- end of event handlers -------------------*/


  /*------------------------- render -----------------------------*/
  render(){
    return (

      <div
        ref = { this.getThisNode }
        className = { styles.lwOuterBox }
        onBlur = { this.handleBlur }
      >
        <div
          className = { styles.lwBox }
        >
          <input
            onClick = { this.handleSearchInputClick }
            onKeyDown = { this.handleSearchInputKeydown }
            onBlur = { this.handleOutsideClick }
            ref = { this.getSearchInput }
            className = { styles.lwSearchText }
            type = "text"
            spellCheck = { false }
            maxLength = { 999 }
            value = { this.state.searchText }
            placeholder = { this.props.placeholder }
            onChange = { this.handleSearchTextChange }
            disabled = { this.props.disabled || null }
            readOnly = { this.props.readonly || null }
          />

          <div
            ref = { this.getULContainer }
            onMouseDown = { this.handleListMouseDown }
            onMouseUp = { this.handleListMouseUp }
            className = { styles.lwList }
            style = {{ maxHeight: "99px", visibility: "hidden" }}
          >
            <ul>
              <li
                className = { styles.lwListSelected }
              >
                ( no item selected )
              </li>
            </ul>
          </div>

          <button
            ref = { this.getDropButton }
            className = { styles.lwButton }
            onClick = { this.handleListButtonClick }
            disabled = { !! this.props.disabled || null }
            tabIndex = '-1'
          >

            <svg
              className = {
                this.state.isListOpen ? styles.lwButtonHide : styles.lwButtonShow
              }
              aria-label = "Open Drop"
              viewBox = "0,0 24,24"
            >
              <polyline
                fill = "none"
                strokeWidth = "2"
                points = "18,9 12,15 6,9"
              >
              </polyline>
            </svg>

            <svg
              className = {
                ! this.state.isListOpen ? styles.lwButtonHide : styles.lwButtonShow
              }
              aria-label = "Close Drop"
              viewBox = "0 0 24 24"
            >
              <polyline
                fill = "none"
                strokeWidth = "2"
                points = "18,9 12,15 6,9"
                transform = "matrix(1 0 0 -1 0 24)"
              >
              </polyline>
            </svg>

          </button>
        </div>

      </div>
    )
  }
  /*------------------------ /render -----------------------------*/

  /*--------------- start of list scroll functions ---------------*/

  // a new list was rendered, clear the scroll state
  listScrollReset = () => {
    const tss = this.state.scroll;
    tss.scrolling = false;
    tss.highlightedLI = null; // break reference to allow GC
  };

  // find the scroll position after the list was recalculated
  // search for an item with data-idx >= the saved idx value
  listScrollInit = ( direction, idx ) => { // 'forward' or 'back'

    const getItemVal = ( theLIs, itemNum ) => {
      return theLIs[ itemNum ].dataset.idx;
    };

    const binSearch = ( items, value ) => {
      let iLow = 0;
      let iHigh = items.length - 1;
      let iSplit;
      while( iLow < iHigh ){
        iSplit = iLow + iHigh >>> 1;
        if( getItemVal( items, iSplit ) < value ){
          iLow = iSplit + 1;
        } else {
          iHigh = iSplit;
        }
      }
      return iSplit;
    };

    this.showTheList(); // ensure the list is visible
    const tss = this.state.scroll;
    let stayInThisLI = false; // scroll functions must not move
    if( tss.scrolling ){
      return stayInThisLI;
    }

    if( typeof tss.idx === 'number' ){
      // smoke test: quick check if the same LI is still there
      const theSameLI = this.getItemsList()
        .querySelector( 'li[data-idx="' + tss.idx.toString() + '"]' );
      if( theSameLI ){
        tss.highlightedLI = theSameLI;
        tss.highlightedLI.classList.add( styles.lwListHighlight );
      } else { // the same LI is gone, search for a near one
        stayInThisLI = true;
        const theLIs = this.getItemsList().querySelectorAll( 'LI' );
        idx = binSearch( theLIs, idx );
        tss.highlightedLI = theLIs[ idx ];
      }
    } else { // no idx: initialize first scroll
      stayInThisLI = true;
      if( ! direction || direction === 'forward' ){
        tss.highlightedLI = this.getItemsList().firstElementChild;
      } else { // direction is 'back'
        tss.highlightedLI = this.getItemsList().lastElementChild;
      }
      tss.idx = Number( tss.highlightedLI.dataset.idx );
    }
    tss.scrolling = true; // kb scrolling is active
    return stayInThisLI;
  };

  // list scroll: highlight the first list/next LI item
  listScrollForward = () => {
    const tss = this.state.scroll;
    const stay = this.listScrollInit( 'forward', tss.idx )

    tss.highlightedLI.classList.remove( styles.lwListHighlight );
    if( ! stay && tss.highlightedLI.nextElementSibling ){ // is not the last LI
      tss.highlightedLI = tss.highlightedLI.nextElementSibling;
    }
    tss.highlightedLI.classList.add( styles.lwListHighlight );
    tss.highlightedLI.scrollIntoView({
      block: 'nearest'
    });
    tss.idx = Number( tss.highlightedLI.dataset.idx );
  };

  /* IN DEVELOPMENT listItemIsVisible = ( li ) => {
    return false;
  }

  // list scroll: highlight the first list/next LI item
  listPageForward = () => {
    const tss = this.state.scroll;
    const stay = this.listScrollInit( 'forward', tss.idx )

    tss.highlightedLI.classList.remove( styles.lwListHighlight );
    if( ! stay && tss.highlightedLI.nextElementSibling ){ // is not the last LI
      do{
        tss.highlightedLI = tss.highlightedLI.nextElementSibling;
      } while( listItemIsVisible( tss.highlightedLI ))
    }
    tss.highlightedLI.classList.add( styles.lwListHighlight );
    tss.highlightedLI.scrollIntoView({ block: 'nearest' });
    tss.idx = Number( tss.highlightedLI.dataset.idx );
  }; */

  // list scroll: highlight the first last/prev LI item
  listScrollBack = () => {
    const tss = this.state.scroll;
    const stay = this.listScrollInit( 'back', tss.idx )

    tss.highlightedLI.classList.remove( styles.lwListHighlight );
    if( ! stay && tss.highlightedLI.previousSibling ){ // is not the first LI
      tss.highlightedLI = tss.highlightedLI.previousSibling;
    }
    tss.highlightedLI.classList.add( styles.lwListHighlight );
    tss.highlightedLI.scrollIntoView({
      block: 'nearest'
    });
    tss.idx = Number( tss.highlightedLI.dataset.idx );
  };

  // list scroll: [enter] selects the current LI
  listScrollSelect = () => {
    const tss = this.state.scroll;
    if( ! tss.scrolling ){
      return;
    }
    tss.highlightedLI.classList.remove( styles.lwListHighlight );
    this.selectListItem( tss.highlightedLI );
  };

  /*---------------- end of list scroll functions ----------------*/


  /*--------------------- service functions ----------------------*/

  // check if is there a valid selection outstanding
  isSelectionOutstanding = () => {
    return ( typeof this.state.valueIndex === 'number' ) &&
      ( this.state.valueIndex >= 0 ) &&
      ( this.state.valueIndex < this.props.listSource.length );
  };

  // get a reference to the display list (lwList is its DIV container)
  getItemsList = () => {
    return this.getULContainer.current.querySelector( 'div[class*="lwList"] ul' );
  };

  // get a reference to the selected list item, or null
  // class="LWSelect_lwListSelected__no4ac"
  getSelectedListItem = () => {
    return this.getULContainer.current.querySelector( 'li[class*="lwListSelected"]' );
  };

  /*-------------------- /service functions ----------------------*/

  /*--------------------------------------------------------------*/
}; // end of LWSelect class

export default LWSelect;

// ✔ 🡆 → ➜ ⯈ ⚡ ⚠️ ⚠ ⚡ 🔍 🗑 * × ✕ ⨉ ＋ 🡨 🡪 🛈 🖉⌄ ⌵ ★ ◀ ∧ ☓


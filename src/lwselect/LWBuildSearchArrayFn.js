// function: builds the search list

const buildSearchArrayFn = ( listSource, listSearchProps ) => {
// listSearchProps: names of the listSource properties that go into the search string
// listSource: the source data

  const n = listSource.length;
  const m = listSearchProps.length;
  const separator = '  ';
  let z;

  // Returns the text of the argument with the accents stripped
  const removeAccents = ( text ) => {
    return text
    .normalize( 'NFD' ) 
    .replace( /[\u0300-\u036f]/g, '' );
  };

  // pre-build empty strings array
  const searchArray = Array( n );
  searchArray.fill && searchArray.fill( '' );

  // function to build a single search string
  const buildSearchField = ( listSourceItem, i ) => {
    z = '';
    for( let j = 0; j < m; j++ ){
      const index = listSearchProps[j];
      const lsii = listSourceItem[ index ];
      lsii && ( z = z + lsii.toString() + separator );
    };
    return removeAccents( z ).toLowerCase();
  };

  // fill searchArray items with text using buildSearchField()
  for( let i = 0; i < n; i++ ){
    searchArray[i] = buildSearchField( listSource[i], i );
  };

  return searchArray;
}

module.exports = buildSearchArrayFn;

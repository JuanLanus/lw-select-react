// import flowersListing from './flowersListing.js';
import carsListing from './carsListing.js';
import citiesListing from './citiesListing.js';
import Select from './lw-select/lw-select.js';


// parameters for the to-be-created Select control named sel
// value: null,      // the selected original-list item
// valueIndex: null, // index of the selected item in the list
// valueText: '',    // the visible text in the search input
// disabled: false (default) | true

// props for the cars example (about 10K items)
const selProps = {
  // The source of the list data, an array of objects.
  listSource: carsListing,

  // A function to be called whenever the selection is changed, either
  // assigned a value, or nullified, due to user actions.
  // Not called on initial values, when the control is initialized.
  // It will be passed 3 args: value, valueIndex and valueText
  onSelectionChanged: ( value, valueIndex, valueText ) => {
    const thePane = document.querySelector( '#result pre' );
    thePane.innerText = ( valueIndex === null ) 
      ? 'no selection'
      : '#' + valueIndex + ' "' + valueText + '"\n'
      + JSON.stringify( value ).replace( /,/g, ', ' ).replace( /:/g, ': ' );
  },

  // A function that returns a composite field to be displayed in the
  // items list.
  // It must be an "arrow function" with two arguments named "item" and "i",
  // like so:
  // ( item, i ) => { '#' + i + ' ' + item.firstName + ' ' + item.secondName }
  // The argument named "item" refers to an item of the listSource array.
  // Instead of a function, it also can be a string with the name of a 
  // property present in the listSource objects array. In this case
  // the referenced field will be copied verbatim.
  listDisplayProps: ( item, i ) => { return item.Year + ' ' + item.Make + ' ' + item.Model },

  // The list of the listSource property names that make a composite search
  // field (lowercased and void of accents) for each item in the listsource
  // array.
  listSearchProps: [ 'Make', 'Model', 'Year', 'Category' ],

  // Placeholder text to be displayed n the search text input (optional)
  placeholder: 'type make, model, year, category',

  // Text to be used instead of the defaulr "no matches found"
  noMatchesMsg: 'no luck, zero matches',

  // Disabled or not
  disabled: false,

  // Text or key/id of a pre-selected item. If there is valueIndex, it overrides
  // valueText. Null if no initial value.
  value: null,
  valueText: null,
  valueIndex: null,

  // Only for the prototype: id of the DOM element where this instance of 
  // lw-select has to be appended.
  targetElementId: 'lwSelectHere',
};

/* const selProps2 = { // another example
  listSource: flowersListing, // the source of the list data, an array (for now)
  listDisplayProps: 'name', // name of the list property that is shown in the list
  listSearchProps: [ 'name', 'title', 'key' ],
  valueText: null,
  valueIndex: null,
  targetElementId: 'lwSelectHere',
}; */

// props for the cities example (about 40K items)
const selProps3 = {
  // Cities example: a list of about 40K cities
  // { city:'Tokyo', lat:'35.6897', long:'139.6922', country:'Japan', iso2:'JP', iso3:'JPN',
  // adminName:'Tōkyō', capital:'primary', population:'37977000', id:'1392685764' },
  listSource: citiesListing,
  onSelectionChanged: ( value, valueIndex, valueText ) => {
    const thePane = document.querySelector( '#result pre' );
    thePane.innerText = ( valueIndex === null ) 
      ? 'no selection'
      : '#' + valueIndex + ' "' + valueText + '"\n'
      + JSON.stringify( value ).replace( /,/g, ', ' ).replace( /:/g, ': ' );
  },
  listDisplayProps: ( item, i ) => { return item.city + ', ' + item.country + ' ' + item.iso2 + '/' + item.iso3 },
  listSearchProps: [ 'city', 'country', 'iso2', 'iso3' ],
  placeholder: 'city, country name, ISO country id',
  disabled: false,
  value: null,
  valueText: null,
  valueIndex: null,
  targetElementId: 'lwSelect3Here',
};

// props for the regions minimal example (7 items)
const selProps2 = {

  // The source of the list data, an array of objects.
  listSource: [
    { num: '1', nombre: 'NOROESTE' },
    { num: '2', nombre: 'LITORAL' },
    { num: '3', nombre: 'CUYO' },
    { num: '4', nombre: 'CENTRO' },
    { num: '5', nombre: 'PROVINCIA DE BUENOS AIRES' },
    { num: '6', nombre: 'CIUDAD DE BUENOS AIRES' },
    { num: '7', nombre: 'PATAGONIA' },
  ],

  // A function to be called whenever the selection is changed
  onSelectionChanged: ( value, valueIndex, valueText ) => {
    console.log( 'selected:', value );
  },

  // A function that returns the string to be displayed in the items list.
  listDisplayProps: ( item, i ) => { return item.num + ' ' + item.nombre },

  // The listSource property names that make a composite search field
  listSearchProps: [ 'num', 'nombre' ],

  targetElementId: 'lwSelect2Here'
};


const initialize = ( event, selProps ) => {
  window.sel = new Select( selProps );
};

const initialize2 = ( event, selProps2 ) => {
  window.sel2 = new Select( selProps2 );
};

const initialize3 = ( event, selProps3 ) => {
  window.sel3 = new Select( selProps3 );
};

const loadHandler = ( event, arg ) => initialize( event, arg );
const loadHandler2 = ( event, arg ) => initialize2( event, arg );
const loadHandler3 = ( event, arg ) => initialize3( event, arg );

window.addEventListener(
  'load',
  ( event ) => {
    loadHandler2( event, selProps2 );
    loadHandler( event, selProps );
    loadHandler3( event, selProps3 );
  }
);

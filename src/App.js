import React from 'react';
import './App.css';
import './lw-root.css';
import LWSelect from './lwselect/LWSelect';
// import flowersListing from './flowersListing.js';
import carsListing from './carsListing.js';
// A 40K LIST: import citiesListing from './citiesListing.js';

function App() {

  const regionsListing = [
    { nivel: '1', nombre: 'NOROESTE' },
    { nivel: '2', nombre: 'LITORAL' },
    { nivel: '3', nombre: 'CUYO' },
    { nivel: '4', nombre: 'CENTRO' },
    { nivel: '5', nombre: 'PROVINCIA DE BUENOS AIRES' },
    { nivel: '6', nombre: 'CIUDAD DE BUENOS AIRES' },
    { nivel: '7', nombre: 'PATAGONIA' },
  ];

  // props for the cars example (about 10K items)
  const carsProps = {
    // The source of the list data, an array of objects.
    listSource: carsListing,

    // A function to be called whenever the selection is changed, either
    // assigned a value, or nullified, due to user actions.
    // Not called on initial values, when the control is initialized.
    // It will be passed 3 args: value, valueIndex and valueText
    onSelectionChanged: ( value, valueIndex, valueText ) => {
      const thePane = document.querySelector( '#result pre' );
      thePane.innerText = ( valueIndex === null ) 
        ? 'no selection'
        : '#' + valueIndex + ' "' + valueText + '"\n'
        + JSON.stringify( value ).replace( /,/g, ', ' ).replace( /:/g, ': ' );
    },

    // A function that returns a composite field to be displayed in the
    // items list.
    // It must be an "arrow function" with two arguments named "item" and "i",
    // like so:
    // ( item, i ) => { '#' + i + ' ' + item.firstName + ' ' + item.secondName }
    // The argument named "item" refers to an item of the listSource array.
    // Instead of a function, it also can be a string with the name of a 
    // property present in the listSource objects array. In this case
    // the referenced field will be copied verbatim.
    listDisplayProps: ( item, i ) => { return item.Year + ' ' + item.Make + ' ' + item.Model },

    // The list of the listSource property names that make a composite search
    // field (lowercased and void of accents) for each item in the listsource
    // array.
    listSearchProps: [ 'Make', 'Model', 'Year', 'Category' ],

    // Placeholder text to be displayed in the search text input (optional)
    placeholder: 'type make, model, year, category',

    // Text to be used instead of the defaulr "no matches found"
    noMatchesMsg: 'no luck, zero matches',

    // Disabled or not
    disabled: false,

    // Text or key/id of a pre-selected item. If there is valueIndex, it overrides
    // valueText. Null if no initial value.
    value: null,
    valueText: null,
    valueIndex: null,
  };


  return (
    <div className="App">

      <div id="container" style={{ position:"relative", padding:"8px",
        minHeight: "100vh", boxSizing: "border-box" }}>
        <h4 style={{ margin:"6px 2px 30px", display:"inline-block" }}
          tabIndex="0">
          <img src="logo192.png" alt="ReactJS logo" style={{ height: "30px" }} />
          lw-select test
        </h4>

        <p>7 items</p>
        <LWSelect 
          // The source of the list data, an array of objects.
          listSource = { regionsListing }

          // A function to be called whenever the selection is changed, either
          // assigned a value, or nullified, due to user actions.
          // Not called on initial values, when the control is initialized.
          // It will be passed 3 args: value, valueIndex and valueText
          onSelectionChanged = { ( value, valueIndex, valueText ) => {
            const thePane = document.querySelector( '#result pre' );
            thePane.innerText = ( valueIndex === null ) 
              ? 'no selection'
              : '#' + valueIndex + ' "' + valueText + '"\n'
              + JSON.stringify( value ).replace( /,/g, ', ' ).replace( /:/g, ': ' );
          } }

          // A function that returns the string to be displayed in the items list.
          // It must be an "arrow function" with two arguments named "item" and "i",
          // like so:
          // ( item, i ) => { '#' + i + ' ' + item.firstName + ' ' + item.secondName }
          // The argument named "item" refers to an item of the listSource array.
          // The "i" argument is the index of the item in the list.
          // Instead of a function, it also can be a string with the name of a 
          // property present in the listSource objects array. In this case
          // the referenced field will be copied verbatim.
          listDisplayProps = {
            ( item, i ) => { return item.nivel + ' ' + item.nombre }
          }

          // The list of the listSource property names that make a composite search
          // field (lowercased and void of accents) for each item in the listsource
          // array.
          listSearchProps = { [ 'nivel', 'nombre' ] }

          // Placeholder text to be displayed n the search text input (optional)
          placeholder = 'nivel nombre'

          // Text or key/id of a pre-selected item. If there is valueIndex, it overrides
          // valueText. Null if no initial value.
          value = { regionsListing[5] }
          valueText = { null }
          valueIndex = { null }

          disabled = { false }

          readonly = { false }
        />


        <p>Same 7 items, readonly</p>
        <LWSelect 
          listSource = { regionsListing }
          onSelectionChanged = { ( value, valueIndex, valueText ) => {
            const thePane = document.querySelector( '#result pre' );
            thePane.innerText = ( valueIndex === null ) 
              ? 'no selection'
              : '#' + valueIndex + ' "' + valueText + '"\n'
              + JSON.stringify( value ).replace( /,/g, ', ' ).replace( /:/g, ': ' );
          } }
          listDisplayProps = {
            ( item, i ) => { return item.nivel + ' ' + item.nombre }
          }
          listSearchProps = { [ 'nivel', 'nombre' ] }
          placeholder = 'nivel nombre'
          value = { regionsListing[5] }
          valueText = { null }
          valueIndex = { null }
          disabled = { false }
          readonly = { true }
        />

        <p>Car models, ~10K items</p>
        <LWSelect 
          { ...carsProps }
          disabled = { false }
        />

        <div id="result">
          <pre>no new selection yet</pre>
        </div>

        <p>Platform select</p>
        <div>
          <input type="text" list="airports" name="airports"
          aria-label="Airports list" style={{ maxWidth: "100%" }} />
            <datalist id="airports">
              <option value="Berlin"></option>
              <option value="Buenos Aires"></option>
              <option value="Los Angeles"></option>
              <option value="Moscow"></option>
              <option value="Paris"></option>
            </datalist>
        </div>

      </div>

    </div>
  );
}

export default App;


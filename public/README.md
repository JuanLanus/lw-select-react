# README PROTO #

This directory (`lw-select/public`) contains an HTML lw-select prototype developed as a proof of concept for the final React component.  

It is brought up by running `npm run proto` from the project directory.  
See issue [#91: HTML: prototype development setup](https://bitbucket.org/JuanLanus/lw-select/issues/91).

The page opened by the proto script is a small form with a title, the lw-select control, a feedback space, and a regular HTML `select` used to compare interactions.  
The lw-select is loaded with a list of car models took from https://www.back4app.com/database that looks like this:  

    const carsListing = [ // excellent data from https://www.back4app.com/database
      { Year:2020, Make:"Audi", Model:"Q3", Category:"SUV" },
      { Year:2020, Make:"Cadillac", Model:"Escalade ESV", Category:"SUV" },
      { Year:2020, Make:"Chevrolet", Model:"Malibu", Category:"Sedan" },
      { Year:2020, Make:"Chevrolet", Model:"Corvette", Category:"Coupe, Convertible" },
      { Year:2020, Make:"Acura", Model:"RLX", Category:"Sedan" },
      { Year:2020, Make:"BMW", Model:"3 Series", Category:"Sedan" },
      { Year:2020, Make:"Chevrolet", Model:"Silverado 2500 HD Crew Cab", Category:"Pickup" },
      { Year:2020, Make:"Chrysler", Model:"Pacifica", Category:"Van/Minivan" },
      { Year:2020, Make:"BMW", Model:"X3", Category:"SUV" },
      ...

Using the known user interactions, you can select a single item of the list by typing any of its search words, in no special order, including data of the field `Category` that is not displayed.  

Also, you can target an item by its position in the list. for example typing "`#3`" will select the 2020 Corvete, as the list is zero-based.  

### Goals ###

The main goal is to achieve high usability. The approach is to expose interactions as close as possible to the normal HTML operation of the `select` element.  

Together with usability comes performance. The response times need to be near instant, even with rather lengthy files. This example has a near-10K items list.  

The other usability is the developer's eperience. Implementing the control should be straightforward, and even if it's not simple (because of the complexity of the task), the steps and its instructions need to be crystal clear so the developers can do it at the first attempt, without frustrations. 

The purpose of this prototype is to approach the lw-select control to get the feeling of how it works and what it does.  

It is clear that it's not feature stuffed. For now it provides basic functionality, easily implemented and undesrtood, and returns decent performance.  

### Starting and props ###

The control is implemented as a class `Select` in the `lw-select.js` file.  

The `Select`, in the prototype, is instantiated with the line:  
  `window.sel = new Select( selProps );`

As the control is intended to be the base for a React component, its parameters are set in an object named `props`.  

The code that brings up the select instance are found in the file `lw-select-run.js`.

It's the code lines, at the end of the `proto.html` file that start with `const selProps`.  

prop name    | description
---------    | -----------
`listSource` | The source of the list data, an array of objects.
`onSelectionChanged` | A function to be called whenever the selection is changed, either assigned a value, or nullified, due to user actions.  
             | Not called on initial values, when the control is initialized.
             | It will be passed 3 args: value, valueIndex and valueText.
             | The `proto.html` file showcases a `onSelectionChanged` function that loads the selected data in the smal pane that's in the window top-right corner. 
`listDisplayProps` | A function that returns the text to be displayed in the items list.
             | It must be an "arrow function" with two arguments named "`item`" and "`i`", like so:
             | `( item, i ) => { '#' + i + ' ' + item.firstName + ' ' + item.secondName }`
             | The argument named "item" refers to an item of the listSource array.
             | Instead of a function, it also can be a string with the name of a simple property present in the listSource objects array. In this case the referenced field will be copied verbatim.
`listSearchProps` | An array with the list of the `listSource` property names that make a composite search field  for each item in the listsource array.
             | The search field is made up of the values of the poperties named hehe, lowercased and void of accents, separated by two spaces.
             | In the running example this fields has:
             | `listSearchProps: [ 'Make', 'Model', 'Year', 'Category' ]`
             | Notice the presence of `Category` that was left out of the display list, so it doesn't whow but can be used to search.
`placeholder` | Placeholder text to be displayed in the search text input (optional)
value*       | See below. Three "initial value" properies. If there is no initial value, set all three to `null`.
             | If there is an initial value, set it in any of the three, possibly more then one.
             | a value is invalid, `lw-select` will complain logging a warning and treat it as if it was `null`.
             | The control will load, in it's `state` object, three fields with the same names as the fields below, containing the data of the selectes item: it's index in the `listSource` array, a reference to the item in the `listSource` array, and the test displayed for the item in the displayed list.
`valueIndex` | Optional pre-selected item. Must be a `Number` that's the index of an item of the `listSource` array, or null.  
`value`      | Optional pre-selected item. Must be a reference to an item of the `listSource` array, or null.  
`valueText`  | Optional pre-selected item. A string equal to one of the strings displayed in the visible list (formatted by `listDisplayProps() ), or null.  
`disabled`   | A value that will be regarded as boolean. If truthy the lw-select intance is rendered disabled. The default is false.  
`targetElementId` | Only for the HTML prototype (not in the ReactJS component): id of the DOM element where this instance of `lw-select` will be appended.  

### Recap

In order to use the `Select`  class, you need to set the data in an array of objects, and to configure a `props` object with the items enumerated above, to tell the control what to display in the visible list, and what to include in the internal search list.  

Once the `Select` artifact is set, it can be operated almost like any regular HTML select control.  
It's main UI is a text input, for the ssearch string, where you can type words or partial words, and see how the list is adjusted to the search text.  
The items in the list are those that matxh all the words or partial words typed.  

While typing the input text has an interim style (blue text in the prototype).  
If the typed text matched no items of the list, the style is changed, in this case it's added a squiggled underscore like that of the lexical correctors of the text editors and a "no matches found" text is displayed instead of the list.  

One the desired item is found, it can be "selected", either by clicking it with the mouse or by selecting it with the up and down arrow keys, and hitting enter.  
After having selected an item, the control displays its description and acquires the  selected   style.  
Selection triggers the associated event, which is the purpose of using these artifacts.  

### Search

Search is done by splitting the search string in words and making a Regex with each.  
The text in the Regexes is the same que user typed, but in lowercase and without accents.  
All the regexes are applied to each of the elements of the search array, retaining the items that match all of them.  

It is also possible to fetch an item by its ordinal number in the list. For example entering "#1234" will return the item number 1234 in the list (zero based indexing).  

Spaces are ommited in the search.  

### Styling

The styles are (mostly) outsourced into CSS variables with rather obviou names.  
All the variable values are defined in the `lw-root.css` file.  
It looks (more or less) like this: 

    :root{
      /* common to everything */
      --font-family:Montserrat, Roboto, sans-serif;
    }

    :root{
      /* general styles for controls */
      --control-height:28px;
      --control-border:1px solid #bbbbbb;
      --control-border-radius:7px;
      --control-inner-border-radius:6px; /* subtract border width */
      --control-bg-color:rgb(51,51,51); /* #333333 */
      --control-bg-color-hover:rgb(80,80,80); /* #505050 */
      --control-text-color:rgb(238, 238, 238); /* #eeeeee */
      --control-text-interim-color:rgb(160,160,255); /* #a0a0ff */
      --control-text-not-valid:underline wavy;
      --control-text-color-hover:white;
      --control-input-text-size:16px;
    }

Some values are defined inside the control CSS file `lw-select.css` that will be migrated to the `.root` class some day.  
